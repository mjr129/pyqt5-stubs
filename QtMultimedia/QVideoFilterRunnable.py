# encoding: utf-8
# module PyQt5.QtMultimedia
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtMultimedia.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QVideoFilterRunnable(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QVideoFilterRunnable()
    QVideoFilterRunnable(QVideoFilterRunnable)
    """
    def run(self, QVideoFrame, QVideoSurfaceFormat, Union, QVideoFilterRunnable_RunFlags=None, QVideoFilterRunnable_RunFlag=None): # real signature unknown; restored from __doc__
        """ run(self, QVideoFrame, QVideoSurfaceFormat, Union[QVideoFilterRunnable.RunFlags, QVideoFilterRunnable.RunFlag]) -> QVideoFrame """
        return QVideoFrame

    def __init__(self, QVideoFilterRunnable=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    LastInChain = 1


