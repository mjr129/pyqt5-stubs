# encoding: utf-8
# module PyQt5.QtMultimedia
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtMultimedia.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QMediaService(__PyQt5_QtCore.QObject):
    """ QMediaService(QObject) """
    def releaseControl(self, QMediaControl): # real signature unknown; restored from __doc__
        """ releaseControl(self, QMediaControl) """
        pass

    def requestControl(self, p_str): # real signature unknown; restored from __doc__
        """ requestControl(self, str) -> QMediaControl """
        return QMediaControl

    def __init__(self, QObject): # real signature unknown; restored from __doc__
        pass


