# encoding: utf-8
# module PyQt5.QtMultimedia
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtMultimedia.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QAudioBuffer(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QAudioBuffer()
    QAudioBuffer(Union[QByteArray, bytes, bytearray], QAudioFormat, startTime: int = -1)
    QAudioBuffer(int, QAudioFormat, startTime: int = -1)
    QAudioBuffer(QAudioBuffer)
    """
    def byteCount(self): # real signature unknown; restored from __doc__
        """ byteCount(self) -> int """
        return 0

    def constData(self): # real signature unknown; restored from __doc__
        """ constData(self) -> sip.voidptr """
        pass

    def data(self): # real signature unknown; restored from __doc__
        """ data(self) -> sip.voidptr """
        pass

    def duration(self): # real signature unknown; restored from __doc__
        """ duration(self) -> int """
        return 0

    def format(self): # real signature unknown; restored from __doc__
        """ format(self) -> QAudioFormat """
        return QAudioFormat

    def frameCount(self): # real signature unknown; restored from __doc__
        """ frameCount(self) -> int """
        return 0

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def sampleCount(self): # real signature unknown; restored from __doc__
        """ sampleCount(self) -> int """
        return 0

    def startTime(self): # real signature unknown; restored from __doc__
        """ startTime(self) -> int """
        return 0

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



