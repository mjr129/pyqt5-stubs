# encoding: utf-8
# module PyQt5.QtMultimedia
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtMultimedia.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QMediaTimeInterval(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QMediaTimeInterval()
    QMediaTimeInterval(int, int)
    QMediaTimeInterval(QMediaTimeInterval)
    """
    def contains(self, p_int): # real signature unknown; restored from __doc__
        """ contains(self, int) -> bool """
        return False

    def end(self): # real signature unknown; restored from __doc__
        """ end(self) -> int """
        return 0

    def isNormal(self): # real signature unknown; restored from __doc__
        """ isNormal(self) -> bool """
        return False

    def normalized(self): # real signature unknown; restored from __doc__
        """ normalized(self) -> QMediaTimeInterval """
        return QMediaTimeInterval

    def start(self): # real signature unknown; restored from __doc__
        """ start(self) -> int """
        return 0

    def translated(self, p_int): # real signature unknown; restored from __doc__
        """ translated(self, int) -> QMediaTimeInterval """
        return QMediaTimeInterval

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


