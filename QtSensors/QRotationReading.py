# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QRotationReading(QSensorReading):
    # no doc
    def setFromEuler(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ setFromEuler(self, float, float, float) """
        pass

    def x(self): # real signature unknown; restored from __doc__
        """ x(self) -> float """
        return 0.0

    def y(self): # real signature unknown; restored from __doc__
        """ y(self) -> float """
        return 0.0

    def z(self): # real signature unknown; restored from __doc__
        """ z(self) -> float """
        return 0.0

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


