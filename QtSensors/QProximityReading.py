# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QProximityReading(QSensorReading):
    # no doc
    def close(self): # real signature unknown; restored from __doc__
        """ close(self) -> bool """
        return False

    def setClose(self, bool): # real signature unknown; restored from __doc__
        """ setClose(self, bool) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


