# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QSensorFilter(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QSensorFilter()
    QSensorFilter(QSensorFilter)
    """
    def filter(self, QSensorReading): # real signature unknown; restored from __doc__
        """ filter(self, QSensorReading) -> bool """
        return False

    def __init__(self, QSensorFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



