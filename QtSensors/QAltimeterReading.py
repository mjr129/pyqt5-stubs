# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QAltimeterReading(QSensorReading):
    # no doc
    def altitude(self): # real signature unknown; restored from __doc__
        """ altitude(self) -> float """
        return 0.0

    def setAltitude(self, p_float): # real signature unknown; restored from __doc__
        """ setAltitude(self, float) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


