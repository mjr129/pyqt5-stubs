# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QTiltReading(QSensorReading):
    # no doc
    def setXRotation(self, p_float): # real signature unknown; restored from __doc__
        """ setXRotation(self, float) """
        pass

    def setYRotation(self, p_float): # real signature unknown; restored from __doc__
        """ setYRotation(self, float) """
        pass

    def xRotation(self): # real signature unknown; restored from __doc__
        """ xRotation(self) -> float """
        return 0.0

    def yRotation(self): # real signature unknown; restored from __doc__
        """ yRotation(self) -> float """
        return 0.0

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


