# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorFilter import QSensorFilter

class QCompassFilter(QSensorFilter):
    """
    QCompassFilter()
    QCompassFilter(QCompassFilter)
    """
    def filter(self, QCompassReading): # real signature unknown; restored from __doc__
        """ filter(self, QCompassReading) -> bool """
        return False

    def __init__(self, QCompassFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


