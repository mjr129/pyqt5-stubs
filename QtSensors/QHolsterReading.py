# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QHolsterReading(QSensorReading):
    # no doc
    def holstered(self): # real signature unknown; restored from __doc__
        """ holstered(self) -> bool """
        return False

    def setHolstered(self, bool): # real signature unknown; restored from __doc__
        """ setHolstered(self, bool) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


