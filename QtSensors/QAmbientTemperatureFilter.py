# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorFilter import QSensorFilter

class QAmbientTemperatureFilter(QSensorFilter):
    """
    QAmbientTemperatureFilter()
    QAmbientTemperatureFilter(QAmbientTemperatureFilter)
    """
    def filter(self, QAmbientTemperatureReading): # real signature unknown; restored from __doc__
        """ filter(self, QAmbientTemperatureReading) -> bool """
        return False

    def __init__(self, QAmbientTemperatureFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


