# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorFilter import QSensorFilter

class QAmbientLightFilter(QSensorFilter):
    """
    QAmbientLightFilter()
    QAmbientLightFilter(QAmbientLightFilter)
    """
    def filter(self, QAmbientLightReading): # real signature unknown; restored from __doc__
        """ filter(self, QAmbientLightReading) -> bool """
        return False

    def __init__(self, QAmbientLightFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


