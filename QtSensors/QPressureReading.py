# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QPressureReading(QSensorReading):
    # no doc
    def pressure(self): # real signature unknown; restored from __doc__
        """ pressure(self) -> float """
        return 0.0

    def setPressure(self, p_float): # real signature unknown; restored from __doc__
        """ setPressure(self, float) """
        pass

    def setTemperature(self, p_float): # real signature unknown; restored from __doc__
        """ setTemperature(self, float) """
        pass

    def temperature(self): # real signature unknown; restored from __doc__
        """ temperature(self) -> float """
        return 0.0

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


