# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensor import QSensor

class QAmbientTemperatureSensor(QSensor):
    """ QAmbientTemperatureSensor(parent: QObject = None) """
    def reading(self): # real signature unknown; restored from __doc__
        """ reading(self) -> QAmbientTemperatureReading """
        return QAmbientTemperatureReading

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


