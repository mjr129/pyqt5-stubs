# encoding: utf-8
# module PyQt5.QtSensors
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtSensors.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QSensorReading import QSensorReading

class QHumidityReading(QSensorReading):
    # no doc
    def absoluteHumidity(self): # real signature unknown; restored from __doc__
        """ absoluteHumidity(self) -> float """
        return 0.0

    def relativeHumidity(self): # real signature unknown; restored from __doc__
        """ relativeHumidity(self) -> float """
        return 0.0

    def setAbsoluteHumidity(self, p_float): # real signature unknown; restored from __doc__
        """ setAbsoluteHumidity(self, float) """
        pass

    def setRelativeHumidity(self, p_float): # real signature unknown; restored from __doc__
        """ setRelativeHumidity(self, float) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


