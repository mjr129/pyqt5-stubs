# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


from .QSGGeometryNode import QSGGeometryNode

class QSGSimpleRectNode(QSGGeometryNode):
    """
    QSGSimpleRectNode(QRectF, Union[QColor, Qt.GlobalColor])
    QSGSimpleRectNode()
    """
    def color(self): # real signature unknown; restored from __doc__
        """ color(self) -> QColor """
        pass

    def rect(self): # real signature unknown; restored from __doc__
        """ rect(self) -> QRectF """
        pass

    def setColor(self, Union, QColor=None, Qt_GlobalColor=None): # real signature unknown; restored from __doc__
        """ setColor(self, Union[QColor, Qt.GlobalColor]) """
        pass

    def setRect(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setRect(self, QRectF)
        setRect(self, float, float, float, float)
        """
        pass

    def __init__(self, QRectF=None, Union=None, QColor=None, Qt_GlobalColor=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


