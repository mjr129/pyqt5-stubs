# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


from .QQuickImageProvider import QQuickImageProvider

class QQuickAsyncImageProvider(QQuickImageProvider):
    """
    QQuickAsyncImageProvider()
    QQuickAsyncImageProvider(QQuickAsyncImageProvider)
    """
    def requestImageResponse(self, p_str, QSize): # real signature unknown; restored from __doc__
        """ requestImageResponse(self, str, QSize) -> QQuickImageResponse """
        return QQuickImageResponse

    def __init__(self, QQuickAsyncImageProvider=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


