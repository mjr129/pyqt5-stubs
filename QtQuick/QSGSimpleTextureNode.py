# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


from .QSGGeometryNode import QSGGeometryNode

class QSGSimpleTextureNode(QSGGeometryNode):
    """ QSGSimpleTextureNode() """
    def filtering(self): # real signature unknown; restored from __doc__
        """ filtering(self) -> QSGTexture.Filtering """
        pass

    def ownsTexture(self): # real signature unknown; restored from __doc__
        """ ownsTexture(self) -> bool """
        return False

    def rect(self): # real signature unknown; restored from __doc__
        """ rect(self) -> QRectF """
        pass

    def setFiltering(self, QSGTexture_Filtering): # real signature unknown; restored from __doc__
        """ setFiltering(self, QSGTexture.Filtering) """
        pass

    def setOwnsTexture(self, bool): # real signature unknown; restored from __doc__
        """ setOwnsTexture(self, bool) """
        pass

    def setRect(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setRect(self, QRectF)
        setRect(self, float, float, float, float)
        """
        pass

    def setSourceRect(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setSourceRect(self, QRectF)
        setSourceRect(self, float, float, float, float)
        """
        pass

    def setTexture(self, QSGTexture): # real signature unknown; restored from __doc__
        """ setTexture(self, QSGTexture) """
        pass

    def setTextureCoordinatesTransform(self, QSGSimpleTextureNode_TextureCoordinatesTransformMode): # real signature unknown; restored from __doc__
        """ setTextureCoordinatesTransform(self, QSGSimpleTextureNode.TextureCoordinatesTransformMode) """
        pass

    def sourceRect(self): # real signature unknown; restored from __doc__
        """ sourceRect(self) -> QRectF """
        pass

    def texture(self): # real signature unknown; restored from __doc__
        """ texture(self) -> QSGTexture """
        return QSGTexture

    def textureCoordinatesTransform(self): # real signature unknown; restored from __doc__
        """ textureCoordinatesTransform(self) -> QSGSimpleTextureNode.TextureCoordinatesTransformMode """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass

    MirrorHorizontally = 1
    MirrorVertically = 2
    NoTransform = 0


