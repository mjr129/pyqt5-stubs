# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


class QQuickImageResponse(__PyQt5_QtCore.QObject):
    """ QQuickImageResponse() """
    def cancel(self): # real signature unknown; restored from __doc__
        """ cancel(self) """
        pass

    def errorString(self): # real signature unknown; restored from __doc__
        """ errorString(self) -> str """
        return ""

    def finished(self): # real signature unknown; restored from __doc__
        """ finished(self) [signal] """
        pass

    def textureFactory(self): # real signature unknown; restored from __doc__
        """ textureFactory(self) -> QQuickTextureFactory """
        return QQuickTextureFactory

    def __init__(self): # real signature unknown; restored from __doc__
        pass


