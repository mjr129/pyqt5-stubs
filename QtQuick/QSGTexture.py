# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


class QSGTexture(__PyQt5_QtCore.QObject):
    """ QSGTexture() """
    def anisotropyLevel(self): # real signature unknown; restored from __doc__
        """ anisotropyLevel(self) -> QSGTexture.AnisotropyLevel """
        pass

    def bind(self): # real signature unknown; restored from __doc__
        """ bind(self) """
        pass

    def convertToNormalizedSourceRect(self, QRectF): # real signature unknown; restored from __doc__
        """ convertToNormalizedSourceRect(self, QRectF) -> QRectF """
        pass

    def filtering(self): # real signature unknown; restored from __doc__
        """ filtering(self) -> QSGTexture.Filtering """
        pass

    def hasAlphaChannel(self): # real signature unknown; restored from __doc__
        """ hasAlphaChannel(self) -> bool """
        return False

    def hasMipmaps(self): # real signature unknown; restored from __doc__
        """ hasMipmaps(self) -> bool """
        return False

    def horizontalWrapMode(self): # real signature unknown; restored from __doc__
        """ horizontalWrapMode(self) -> QSGTexture.WrapMode """
        pass

    def isAtlasTexture(self): # real signature unknown; restored from __doc__
        """ isAtlasTexture(self) -> bool """
        return False

    def mipmapFiltering(self): # real signature unknown; restored from __doc__
        """ mipmapFiltering(self) -> QSGTexture.Filtering """
        pass

    def normalizedTextureSubRect(self): # real signature unknown; restored from __doc__
        """ normalizedTextureSubRect(self) -> QRectF """
        pass

    def removedFromAtlas(self): # real signature unknown; restored from __doc__
        """ removedFromAtlas(self) -> QSGTexture """
        return QSGTexture

    def setAnisotropyLevel(self, QSGTexture_AnisotropyLevel): # real signature unknown; restored from __doc__
        """ setAnisotropyLevel(self, QSGTexture.AnisotropyLevel) """
        pass

    def setFiltering(self, QSGTexture_Filtering): # real signature unknown; restored from __doc__
        """ setFiltering(self, QSGTexture.Filtering) """
        pass

    def setHorizontalWrapMode(self, QSGTexture_WrapMode): # real signature unknown; restored from __doc__
        """ setHorizontalWrapMode(self, QSGTexture.WrapMode) """
        pass

    def setMipmapFiltering(self, QSGTexture_Filtering): # real signature unknown; restored from __doc__
        """ setMipmapFiltering(self, QSGTexture.Filtering) """
        pass

    def setVerticalWrapMode(self, QSGTexture_WrapMode): # real signature unknown; restored from __doc__
        """ setVerticalWrapMode(self, QSGTexture.WrapMode) """
        pass

    def textureId(self): # real signature unknown; restored from __doc__
        """ textureId(self) -> int """
        return 0

    def textureSize(self): # real signature unknown; restored from __doc__
        """ textureSize(self) -> QSize """
        pass

    def updateBindOptions(self, force=False): # real signature unknown; restored from __doc__
        """ updateBindOptions(self, force: bool = False) """
        pass

    def verticalWrapMode(self): # real signature unknown; restored from __doc__
        """ verticalWrapMode(self) -> QSGTexture.WrapMode """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass

    Anisotropy16x = 4
    Anisotropy2x = 1
    Anisotropy4x = 2
    Anisotropy8x = 3
    AnisotropyNone = 0
    ClampToEdge = 1
    Linear = 2
    Nearest = 1
    Repeat = 0


