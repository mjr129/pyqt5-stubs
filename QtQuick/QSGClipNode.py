# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


from .QSGBasicGeometryNode import QSGBasicGeometryNode

class QSGClipNode(QSGBasicGeometryNode):
    """ QSGClipNode() """
    def clipRect(self): # real signature unknown; restored from __doc__
        """ clipRect(self) -> QRectF """
        pass

    def isRectangular(self): # real signature unknown; restored from __doc__
        """ isRectangular(self) -> bool """
        return False

    def setClipRect(self, QRectF): # real signature unknown; restored from __doc__
        """ setClipRect(self, QRectF) """
        pass

    def setIsRectangular(self, bool): # real signature unknown; restored from __doc__
        """ setIsRectangular(self, bool) """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass


