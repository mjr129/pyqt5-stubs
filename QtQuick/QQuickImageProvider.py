# encoding: utf-8
# module PyQt5.QtQuick
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtQuick.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml


class QQuickImageProvider(__PyQt5_QtQml.QQmlImageProviderBase):
    """
    QQuickImageProvider(QQmlImageProviderBase.ImageType, flags: Union[QQmlImageProviderBase.Flags, QQmlImageProviderBase.Flag] = 0)
    QQuickImageProvider(QQuickImageProvider)
    """
    def flags(self): # real signature unknown; restored from __doc__
        """ flags(self) -> QQmlImageProviderBase.Flags """
        pass

    def imageType(self): # real signature unknown; restored from __doc__
        """ imageType(self) -> QQmlImageProviderBase.ImageType """
        pass

    def requestImage(self, p_str, QSize): # real signature unknown; restored from __doc__
        """ requestImage(self, str, QSize) -> Tuple[QImage, QSize] """
        pass

    def requestPixmap(self, p_str, QSize): # real signature unknown; restored from __doc__
        """ requestPixmap(self, str, QSize) -> Tuple[QPixmap, QSize] """
        pass

    def requestTexture(self, p_str, QSize): # real signature unknown; restored from __doc__
        """ requestTexture(self, str, QSize) -> Tuple[QQuickTextureFactory, QSize] """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


