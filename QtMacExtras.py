# encoding: utf-8
# module PyQt5.QtMacExtras
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtMacExtras.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


# functions

def qRegisterDraggedTypes(Iterable, p_str=None): # real signature unknown; restored from __doc__
    """ qRegisterDraggedTypes(Iterable[str]) """
    pass

# classes

class NSToolbar(): # skipped bases: <class 'sip.simplewrapper'>
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class NSToolbarItem(): # skipped bases: <class 'sip.simplewrapper'>
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class QMacPasteboardMime(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QMacPasteboardMime(int)
    QMacPasteboardMime(QMacPasteboardMime)
    """
    def canConvert(self, p_str, p_str_1): # real signature unknown; restored from __doc__
        """ canConvert(self, str, str) -> bool """
        return False

    def convertFromMime(self, p_str, Any, p_str_1): # real signature unknown; restored from __doc__
        """ convertFromMime(self, str, Any, str) -> List[QByteArray] """
        return []

    def convertorName(self): # real signature unknown; restored from __doc__
        """ convertorName(self) -> str """
        return ""

    def convertToMime(self, p_str, Iterable, Union=None, QByteArray=None, bytes=None, bytearray=None, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """ convertToMime(self, str, Iterable[Union[QByteArray, bytes, bytearray]], str) -> Any """
        pass

    def count(self, QMimeData): # real signature unknown; restored from __doc__
        """ count(self, QMimeData) -> int """
        return 0

    def flavorFor(self, p_str): # real signature unknown; restored from __doc__
        """ flavorFor(self, str) -> str """
        return ""

    def mimeFor(self, p_str): # real signature unknown; restored from __doc__
        """ mimeFor(self, str) -> str """
        return ""

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    MIME_ALL = 3
    MIME_CLIP = 2
    MIME_DND = 1
    MIME_QT3_CONVERTOR = 8
    MIME_QT_CONVERTOR = 4


class QMacToolBar(__PyQt5_QtCore.QObject):
    """
    QMacToolBar(parent: QObject = None)
    QMacToolBar(str, parent: QObject = None)
    """
    def addAllowedItem(self, QIcon, p_str): # real signature unknown; restored from __doc__
        """ addAllowedItem(self, QIcon, str) -> QMacToolBarItem """
        return QMacToolBarItem

    def addItem(self, QIcon, p_str): # real signature unknown; restored from __doc__
        """ addItem(self, QIcon, str) -> QMacToolBarItem """
        return QMacToolBarItem

    def addSeparator(self): # real signature unknown; restored from __doc__
        """ addSeparator(self) """
        pass

    def allowedItems(self): # real signature unknown; restored from __doc__
        """ allowedItems(self) -> List[QMacToolBarItem] """
        return []

    def attachToWindow(self, QWindow): # real signature unknown; restored from __doc__
        """ attachToWindow(self, QWindow) """
        pass

    def detachFromWindow(self): # real signature unknown; restored from __doc__
        """ detachFromWindow(self) """
        pass

    def items(self): # real signature unknown; restored from __doc__
        """ items(self) -> List[QMacToolBarItem] """
        return []

    def nativeToolbar(self): # real signature unknown; restored from __doc__
        """ nativeToolbar(self) -> NSToolbar """
        return NSToolbar

    def setAllowedItems(self, Iterable, QMacToolBarItem=None): # real signature unknown; restored from __doc__
        """ setAllowedItems(self, Iterable[QMacToolBarItem]) """
        pass

    def setItems(self, p_object): # real signature unknown; restored from __doc__
        """ setItems(self, object) """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


class QMacToolBarItem(__PyQt5_QtCore.QObject):
    """ QMacToolBarItem(parent: QObject = None) """
    def activated(self): # real signature unknown; restored from __doc__
        """ activated(self) [signal] """
        pass

    def icon(self): # real signature unknown; restored from __doc__
        """ icon(self) -> QIcon """
        pass

    def nativeToolBarItem(self): # real signature unknown; restored from __doc__
        """ nativeToolBarItem(self) -> NSToolbarItem """
        return NSToolbarItem

    def selectable(self): # real signature unknown; restored from __doc__
        """ selectable(self) -> bool """
        return False

    def setIcon(self, QIcon): # real signature unknown; restored from __doc__
        """ setIcon(self, QIcon) """
        pass

    def setSelectable(self, bool): # real signature unknown; restored from __doc__
        """ setSelectable(self, bool) """
        pass

    def setStandardItem(self, QMacToolBarItem_StandardItem): # real signature unknown; restored from __doc__
        """ setStandardItem(self, QMacToolBarItem.StandardItem) """
        pass

    def setText(self, p_str): # real signature unknown; restored from __doc__
        """ setText(self, str) """
        pass

    def standardItem(self): # real signature unknown; restored from __doc__
        """ standardItem(self) -> QMacToolBarItem.StandardItem """
        pass

    def text(self): # real signature unknown; restored from __doc__
        """ text(self) -> str """
        return ""

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass

    FlexibleSpace = 2
    NoStandardItem = 0
    Space = 1


class QtMac(): # skipped bases: <class 'sip.simplewrapper'>
    # no doc
    def badgeLabelText(self): # real signature unknown; restored from __doc__
        """ badgeLabelText() -> str """
        return ""

    def isMainWindow(self, QWindow): # real signature unknown; restored from __doc__
        """ isMainWindow(QWindow) -> bool """
        return False

    def setBadgeLabelText(self, p_str): # real signature unknown; restored from __doc__
        """ setBadgeLabelText(str) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values



