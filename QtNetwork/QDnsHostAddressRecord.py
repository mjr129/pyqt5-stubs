# encoding: utf-8
# module PyQt5.QtNetwork
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtNetwork.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QDnsHostAddressRecord(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QDnsHostAddressRecord()
    QDnsHostAddressRecord(QDnsHostAddressRecord)
    """
    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def swap(self, QDnsHostAddressRecord): # real signature unknown; restored from __doc__
        """ swap(self, QDnsHostAddressRecord) """
        pass

    def timeToLive(self): # real signature unknown; restored from __doc__
        """ timeToLive(self) -> int """
        return 0

    def value(self): # real signature unknown; restored from __doc__
        """ value(self) -> QHostAddress """
        return QHostAddress

    def __init__(self, QDnsHostAddressRecord=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



