# encoding: utf-8
# module PyQt5.QtNetwork
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtNetwork.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QNetworkAddressEntry(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QNetworkAddressEntry()
    QNetworkAddressEntry(QNetworkAddressEntry)
    """
    def broadcast(self): # real signature unknown; restored from __doc__
        """ broadcast(self) -> QHostAddress """
        return QHostAddress

    def ip(self): # real signature unknown; restored from __doc__
        """ ip(self) -> QHostAddress """
        return QHostAddress

    def netmask(self): # real signature unknown; restored from __doc__
        """ netmask(self) -> QHostAddress """
        return QHostAddress

    def prefixLength(self): # real signature unknown; restored from __doc__
        """ prefixLength(self) -> int """
        return 0

    def setBroadcast(self, Union, QHostAddress=None, QHostAddress_SpecialAddress=None): # real signature unknown; restored from __doc__
        """ setBroadcast(self, Union[QHostAddress, QHostAddress.SpecialAddress]) """
        pass

    def setIp(self, Union, QHostAddress=None, QHostAddress_SpecialAddress=None): # real signature unknown; restored from __doc__
        """ setIp(self, Union[QHostAddress, QHostAddress.SpecialAddress]) """
        pass

    def setNetmask(self, Union, QHostAddress=None, QHostAddress_SpecialAddress=None): # real signature unknown; restored from __doc__
        """ setNetmask(self, Union[QHostAddress, QHostAddress.SpecialAddress]) """
        pass

    def setPrefixLength(self, p_int): # real signature unknown; restored from __doc__
        """ setPrefixLength(self, int) """
        pass

    def swap(self, QNetworkAddressEntry): # real signature unknown; restored from __doc__
        """ swap(self, QNetworkAddressEntry) """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QNetworkAddressEntry=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


