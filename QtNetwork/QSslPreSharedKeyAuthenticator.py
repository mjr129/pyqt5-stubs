# encoding: utf-8
# module PyQt5.QtNetwork
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtNetwork.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QSslPreSharedKeyAuthenticator(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QSslPreSharedKeyAuthenticator()
    QSslPreSharedKeyAuthenticator(QSslPreSharedKeyAuthenticator)
    """
    def identity(self): # real signature unknown; restored from __doc__
        """ identity(self) -> QByteArray """
        pass

    def identityHint(self): # real signature unknown; restored from __doc__
        """ identityHint(self) -> QByteArray """
        pass

    def maximumIdentityLength(self): # real signature unknown; restored from __doc__
        """ maximumIdentityLength(self) -> int """
        return 0

    def maximumPreSharedKeyLength(self): # real signature unknown; restored from __doc__
        """ maximumPreSharedKeyLength(self) -> int """
        return 0

    def preSharedKey(self): # real signature unknown; restored from __doc__
        """ preSharedKey(self) -> QByteArray """
        pass

    def setIdentity(self, Union, QByteArray=None, bytes=None, bytearray=None): # real signature unknown; restored from __doc__
        """ setIdentity(self, Union[QByteArray, bytes, bytearray]) """
        pass

    def setPreSharedKey(self, Union, QByteArray=None, bytes=None, bytearray=None): # real signature unknown; restored from __doc__
        """ setPreSharedKey(self, Union[QByteArray, bytes, bytearray]) """
        pass

    def swap(self, QSslPreSharedKeyAuthenticator): # real signature unknown; restored from __doc__
        """ swap(self, QSslPreSharedKeyAuthenticator) """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QSslPreSharedKeyAuthenticator=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


