# encoding: utf-8
# module PyQt5.QtNetwork
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtNetwork.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QAbstractSocket import QAbstractSocket

class QTcpSocket(QAbstractSocket):
    """ QTcpSocket(parent: QObject = None) """
    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


