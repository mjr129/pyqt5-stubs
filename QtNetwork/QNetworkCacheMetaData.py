# encoding: utf-8
# module PyQt5.QtNetwork
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtNetwork.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QNetworkCacheMetaData(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QNetworkCacheMetaData()
    QNetworkCacheMetaData(QNetworkCacheMetaData)
    """
    def attributes(self): # real signature unknown; restored from __doc__
        """ attributes(self) -> Dict[QNetworkRequest.Attribute, Any] """
        return {}

    def expirationDate(self): # real signature unknown; restored from __doc__
        """ expirationDate(self) -> QDateTime """
        pass

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def lastModified(self): # real signature unknown; restored from __doc__
        """ lastModified(self) -> QDateTime """
        pass

    def rawHeaders(self): # real signature unknown; restored from __doc__
        """ rawHeaders(self) -> object """
        return object()

    def saveToDisk(self): # real signature unknown; restored from __doc__
        """ saveToDisk(self) -> bool """
        return False

    def setAttributes(self, Dict, QNetworkRequest_Attribute=None, Any=None): # real signature unknown; restored from __doc__
        """ setAttributes(self, Dict[QNetworkRequest.Attribute, Any]) """
        pass

    def setExpirationDate(self, Union, QDateTime=None, datetime_datetime=None): # real signature unknown; restored from __doc__
        """ setExpirationDate(self, Union[QDateTime, datetime.datetime]) """
        pass

    def setLastModified(self, Union, QDateTime=None, datetime_datetime=None): # real signature unknown; restored from __doc__
        """ setLastModified(self, Union[QDateTime, datetime.datetime]) """
        pass

    def setRawHeaders(self, p_object): # real signature unknown; restored from __doc__
        """ setRawHeaders(self, object) """
        pass

    def setSaveToDisk(self, bool): # real signature unknown; restored from __doc__
        """ setSaveToDisk(self, bool) """
        pass

    def setUrl(self, QUrl): # real signature unknown; restored from __doc__
        """ setUrl(self, QUrl) """
        pass

    def swap(self, QNetworkCacheMetaData): # real signature unknown; restored from __doc__
        """ swap(self, QNetworkCacheMetaData) """
        pass

    def url(self): # real signature unknown; restored from __doc__
        """ url(self) -> QUrl """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QNetworkCacheMetaData=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


