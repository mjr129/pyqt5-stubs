# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QValidator(__PyQt5_QtCore.QObject):
    """ QValidator(parent: QObject = None) """
    def changed(self): # real signature unknown; restored from __doc__
        """ changed(self) [signal] """
        pass

    def fixup(self, p_str): # real signature unknown; restored from __doc__
        """ fixup(self, str) -> str """
        return ""

    def locale(self): # real signature unknown; restored from __doc__
        """ locale(self) -> QLocale """
        pass

    def setLocale(self, QLocale): # real signature unknown; restored from __doc__
        """ setLocale(self, QLocale) """
        pass

    def validate(self, p_str, p_int): # real signature unknown; restored from __doc__
        """ validate(self, str, int) -> Tuple[QValidator.State, str, int] """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass

    Acceptable = 2
    Intermediate = 1
    Invalid = 0


