# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QValidator import QValidator

class QRegExpValidator(QValidator):
    """
    QRegExpValidator(parent: QObject = None)
    QRegExpValidator(QRegExp, parent: QObject = None)
    """
    def regExp(self): # real signature unknown; restored from __doc__
        """ regExp(self) -> QRegExp """
        pass

    def setRegExp(self, QRegExp): # real signature unknown; restored from __doc__
        """ setRegExp(self, QRegExp) """
        pass

    def validate(self, p_str, p_int): # real signature unknown; restored from __doc__
        """ validate(self, str, int) -> Tuple[QValidator.State, str, int] """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


