# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QTextFragment(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QTextFragment()
    QTextFragment(QTextFragment)
    """
    def charFormat(self): # real signature unknown; restored from __doc__
        """ charFormat(self) -> QTextCharFormat """
        return QTextCharFormat

    def charFormatIndex(self): # real signature unknown; restored from __doc__
        """ charFormatIndex(self) -> int """
        return 0

    def contains(self, p_int): # real signature unknown; restored from __doc__
        """ contains(self, int) -> bool """
        return False

    def glyphRuns(self, from_=-1, length=-1): # real signature unknown; restored from __doc__
        """ glyphRuns(self, from_: int = -1, length: int = -1) -> List[QGlyphRun] """
        return []

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def length(self): # real signature unknown; restored from __doc__
        """ length(self) -> int """
        return 0

    def position(self): # real signature unknown; restored from __doc__
        """ position(self) -> int """
        return 0

    def text(self): # real signature unknown; restored from __doc__
        """ text(self) -> str """
        return ""

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QTextFragment=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


