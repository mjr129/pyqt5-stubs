# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QInputEvent(__PyQt5_QtCore.QEvent):
    # no doc
    def modifiers(self): # real signature unknown; restored from __doc__
        """ modifiers(self) -> Qt.KeyboardModifiers """
        pass

    def setTimestamp(self, p_int): # real signature unknown; restored from __doc__
        """ setTimestamp(self, int) """
        pass

    def timestamp(self): # real signature unknown; restored from __doc__
        """ timestamp(self) -> int """
        return 0

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


