# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QInputMethodQueryEvent(__PyQt5_QtCore.QEvent):
    """
    QInputMethodQueryEvent(Union[Qt.InputMethodQueries, Qt.InputMethodQuery])
    QInputMethodQueryEvent(QInputMethodQueryEvent)
    """
    def queries(self): # real signature unknown; restored from __doc__
        """ queries(self) -> Qt.InputMethodQueries """
        pass

    def setValue(self, Qt_InputMethodQuery, Any): # real signature unknown; restored from __doc__
        """ setValue(self, Qt.InputMethodQuery, Any) """
        pass

    def value(self, Qt_InputMethodQuery): # real signature unknown; restored from __doc__
        """ value(self, Qt.InputMethodQuery) -> Any """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


