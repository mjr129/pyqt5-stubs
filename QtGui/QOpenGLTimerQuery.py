# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QOpenGLTimerQuery(__PyQt5_QtCore.QObject):
    """ QOpenGLTimerQuery(parent: QObject = None) """
    def begin(self): # real signature unknown; restored from __doc__
        """ begin(self) """
        pass

    def create(self): # real signature unknown; restored from __doc__
        """ create(self) -> bool """
        return False

    def destroy(self): # real signature unknown; restored from __doc__
        """ destroy(self) """
        pass

    def end(self): # real signature unknown; restored from __doc__
        """ end(self) """
        pass

    def isCreated(self): # real signature unknown; restored from __doc__
        """ isCreated(self) -> bool """
        return False

    def isResultAvailable(self): # real signature unknown; restored from __doc__
        """ isResultAvailable(self) -> bool """
        return False

    def objectId(self): # real signature unknown; restored from __doc__
        """ objectId(self) -> int """
        return 0

    def recordTimestamp(self): # real signature unknown; restored from __doc__
        """ recordTimestamp(self) """
        pass

    def waitForResult(self): # real signature unknown; restored from __doc__
        """ waitForResult(self) -> int """
        return 0

    def waitForTimestamp(self): # real signature unknown; restored from __doc__
        """ waitForTimestamp(self) -> int """
        return 0

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


