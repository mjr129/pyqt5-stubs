# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QMoveEvent(__PyQt5_QtCore.QEvent):
    """
    QMoveEvent(QPoint, QPoint)
    QMoveEvent(QMoveEvent)
    """
    def oldPos(self): # real signature unknown; restored from __doc__
        """ oldPos(self) -> QPoint """
        pass

    def pos(self): # real signature unknown; restored from __doc__
        """ pos(self) -> QPoint """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


