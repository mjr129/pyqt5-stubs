# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QTextTableCell(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QTextTableCell()
    QTextTableCell(QTextTableCell)
    """
    def column(self): # real signature unknown; restored from __doc__
        """ column(self) -> int """
        return 0

    def columnSpan(self): # real signature unknown; restored from __doc__
        """ columnSpan(self) -> int """
        return 0

    def firstCursorPosition(self): # real signature unknown; restored from __doc__
        """ firstCursorPosition(self) -> QTextCursor """
        return QTextCursor

    def format(self): # real signature unknown; restored from __doc__
        """ format(self) -> QTextCharFormat """
        return QTextCharFormat

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def lastCursorPosition(self): # real signature unknown; restored from __doc__
        """ lastCursorPosition(self) -> QTextCursor """
        return QTextCursor

    def row(self): # real signature unknown; restored from __doc__
        """ row(self) -> int """
        return 0

    def rowSpan(self): # real signature unknown; restored from __doc__
        """ rowSpan(self) -> int """
        return 0

    def setFormat(self, QTextCharFormat): # real signature unknown; restored from __doc__
        """ setFormat(self, QTextCharFormat) """
        pass

    def tableCellFormatIndex(self): # real signature unknown; restored from __doc__
        """ tableCellFormatIndex(self) -> int """
        return 0

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QTextTableCell=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


