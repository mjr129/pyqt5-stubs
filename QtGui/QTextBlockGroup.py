# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QTextObject import QTextObject

class QTextBlockGroup(QTextObject):
    """ QTextBlockGroup(QTextDocument) """
    def blockFormatChanged(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockFormatChanged(self, QTextBlock) """
        pass

    def blockInserted(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockInserted(self, QTextBlock) """
        pass

    def blockList(self): # real signature unknown; restored from __doc__
        """ blockList(self) -> object """
        return object()

    def blockRemoved(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockRemoved(self, QTextBlock) """
        pass

    def __init__(self, QTextDocument): # real signature unknown; restored from __doc__
        pass


