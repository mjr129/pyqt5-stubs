# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QValidator import QValidator

class QRegularExpressionValidator(QValidator):
    """
    QRegularExpressionValidator(parent: QObject = None)
    QRegularExpressionValidator(QRegularExpression, parent: QObject = None)
    """
    def regularExpression(self): # real signature unknown; restored from __doc__
        """ regularExpression(self) -> QRegularExpression """
        pass

    def setRegularExpression(self, QRegularExpression): # real signature unknown; restored from __doc__
        """ setRegularExpression(self, QRegularExpression) """
        pass

    def validate(self, p_str, p_int): # real signature unknown; restored from __doc__
        """ validate(self, str, int) -> Tuple[QValidator.State, str, int] """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


