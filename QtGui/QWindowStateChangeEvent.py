# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QWindowStateChangeEvent(__PyQt5_QtCore.QEvent):
    # no doc
    def oldState(self): # real signature unknown; restored from __doc__
        """ oldState(self) -> Qt.WindowStates """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


