# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QDesktopServices(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QDesktopServices()
    QDesktopServices(QDesktopServices)
    """
    def openUrl(self, QUrl): # real signature unknown; restored from __doc__
        """ openUrl(QUrl) -> bool """
        return False

    def setUrlHandler(self, p_str, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setUrlHandler(str, QObject, str)
        setUrlHandler(str, Callable[[], None])
        """
        pass

    def unsetUrlHandler(self, p_str): # real signature unknown; restored from __doc__
        """ unsetUrlHandler(str) """
        pass

    def __init__(self, QDesktopServices=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



