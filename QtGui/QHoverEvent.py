# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QInputEvent import QInputEvent

class QHoverEvent(QInputEvent):
    """
    QHoverEvent(QEvent.Type, Union[QPointF, QPoint], Union[QPointF, QPoint], modifiers: Union[Qt.KeyboardModifiers, Qt.KeyboardModifier] = Qt.NoModifier)
    QHoverEvent(QHoverEvent)
    """
    def oldPos(self): # real signature unknown; restored from __doc__
        """ oldPos(self) -> QPoint """
        pass

    def oldPosF(self): # real signature unknown; restored from __doc__
        """ oldPosF(self) -> QPointF """
        pass

    def pos(self): # real signature unknown; restored from __doc__
        """ pos(self) -> QPoint """
        pass

    def posF(self): # real signature unknown; restored from __doc__
        """ posF(self) -> QPointF """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


