# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QFocusEvent(__PyQt5_QtCore.QEvent):
    """
    QFocusEvent(QEvent.Type, reason: Qt.FocusReason = Qt.OtherFocusReason)
    QFocusEvent(QFocusEvent)
    """
    def gotFocus(self): # real signature unknown; restored from __doc__
        """ gotFocus(self) -> bool """
        return False

    def lostFocus(self): # real signature unknown; restored from __doc__
        """ lostFocus(self) -> bool """
        return False

    def reason(self): # real signature unknown; restored from __doc__
        """ reason(self) -> Qt.FocusReason """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


