# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QTextCharFormat import QTextCharFormat

class QTextImageFormat(QTextCharFormat):
    """
    QTextImageFormat()
    QTextImageFormat(QTextImageFormat)
    """
    def height(self): # real signature unknown; restored from __doc__
        """ height(self) -> float """
        return 0.0

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def setHeight(self, p_float): # real signature unknown; restored from __doc__
        """ setHeight(self, float) """
        pass

    def setName(self, p_str): # real signature unknown; restored from __doc__
        """ setName(self, str) """
        pass

    def setWidth(self, p_float): # real signature unknown; restored from __doc__
        """ setWidth(self, float) """
        pass

    def width(self): # real signature unknown; restored from __doc__
        """ width(self) -> float """
        return 0.0

    def __init__(self, QTextImageFormat=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


