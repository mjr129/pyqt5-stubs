# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QGradient import QGradient

class QConicalGradient(QGradient):
    """
    QConicalGradient()
    QConicalGradient(Union[QPointF, QPoint], float)
    QConicalGradient(float, float, float)
    QConicalGradient(QConicalGradient)
    """
    def angle(self): # real signature unknown; restored from __doc__
        """ angle(self) -> float """
        return 0.0

    def center(self): # real signature unknown; restored from __doc__
        """ center(self) -> QPointF """
        pass

    def setAngle(self, p_float): # real signature unknown; restored from __doc__
        """ setAngle(self, float) """
        pass

    def setCenter(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setCenter(self, Union[QPointF, QPoint])
        setCenter(self, float, float)
        """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


