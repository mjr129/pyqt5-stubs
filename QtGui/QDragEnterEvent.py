# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QDragMoveEvent import QDragMoveEvent

class QDragEnterEvent(QDragMoveEvent):
    """
    QDragEnterEvent(QPoint, Union[Qt.DropActions, Qt.DropAction], QMimeData, Union[Qt.MouseButtons, Qt.MouseButton], Union[Qt.KeyboardModifiers, Qt.KeyboardModifier])
    QDragEnterEvent(QDragEnterEvent)
    """
    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


