# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QOpenGLVersionProfile(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QOpenGLVersionProfile()
    QOpenGLVersionProfile(QSurfaceFormat)
    QOpenGLVersionProfile(QOpenGLVersionProfile)
    """
    def hasProfiles(self): # real signature unknown; restored from __doc__
        """ hasProfiles(self) -> bool """
        return False

    def isLegacyVersion(self): # real signature unknown; restored from __doc__
        """ isLegacyVersion(self) -> bool """
        return False

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def profile(self): # real signature unknown; restored from __doc__
        """ profile(self) -> QSurfaceFormat.OpenGLContextProfile """
        pass

    def setProfile(self, QSurfaceFormat_OpenGLContextProfile): # real signature unknown; restored from __doc__
        """ setProfile(self, QSurfaceFormat.OpenGLContextProfile) """
        pass

    def setVersion(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ setVersion(self, int, int) """
        pass

    def version(self): # real signature unknown; restored from __doc__
        """ version(self) -> Tuple[int, int] """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


