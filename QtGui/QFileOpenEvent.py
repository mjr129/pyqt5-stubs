# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QFileOpenEvent(__PyQt5_QtCore.QEvent):
    # no doc
    def file(self): # real signature unknown; restored from __doc__
        """ file(self) -> str """
        return ""

    def openFile(self, QFile, Union, QIODevice_OpenMode=None, QIODevice_OpenModeFlag=None): # real signature unknown; restored from __doc__
        """ openFile(self, QFile, Union[QIODevice.OpenMode, QIODevice.OpenModeFlag]) -> bool """
        return False

    def url(self): # real signature unknown; restored from __doc__
        """ url(self) -> QUrl """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


