# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QResizeEvent(__PyQt5_QtCore.QEvent):
    """
    QResizeEvent(QSize, QSize)
    QResizeEvent(QResizeEvent)
    """
    def oldSize(self): # real signature unknown; restored from __doc__
        """ oldSize(self) -> QSize """
        pass

    def size(self): # real signature unknown; restored from __doc__
        """ size(self) -> QSize """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


