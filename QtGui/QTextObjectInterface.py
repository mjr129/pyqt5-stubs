# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QTextObjectInterface(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QTextObjectInterface()
    QTextObjectInterface(QTextObjectInterface)
    """
    def drawObject(self, QPainter, QRectF, QTextDocument, p_int, QTextFormat): # real signature unknown; restored from __doc__
        """ drawObject(self, QPainter, QRectF, QTextDocument, int, QTextFormat) """
        pass

    def intrinsicSize(self, QTextDocument, p_int, QTextFormat): # real signature unknown; restored from __doc__
        """ intrinsicSize(self, QTextDocument, int, QTextFormat) -> QSizeF """
        pass

    def __init__(self, QTextObjectInterface=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



