# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QTextBlockGroup import QTextBlockGroup

class QTextList(QTextBlockGroup):
    """ QTextList(QTextDocument) """
    def add(self, QTextBlock): # real signature unknown; restored from __doc__
        """ add(self, QTextBlock) """
        pass

    def count(self): # real signature unknown; restored from __doc__
        """ count(self) -> int """
        return 0

    def format(self): # real signature unknown; restored from __doc__
        """ format(self) -> QTextListFormat """
        return QTextListFormat

    def item(self, p_int): # real signature unknown; restored from __doc__
        """ item(self, int) -> QTextBlock """
        return QTextBlock

    def itemNumber(self, QTextBlock): # real signature unknown; restored from __doc__
        """ itemNumber(self, QTextBlock) -> int """
        return 0

    def itemText(self, QTextBlock): # real signature unknown; restored from __doc__
        """ itemText(self, QTextBlock) -> str """
        return ""

    def remove(self, QTextBlock): # real signature unknown; restored from __doc__
        """ remove(self, QTextBlock) """
        pass

    def removeItem(self, p_int): # real signature unknown; restored from __doc__
        """ removeItem(self, int) """
        pass

    def setFormat(self, QTextListFormat): # real signature unknown; restored from __doc__
        """ setFormat(self, QTextListFormat) """
        pass

    def __init__(self, QTextDocument): # real signature unknown; restored from __doc__
        pass

    def __len__(self, *args, **kwargs): # real signature unknown
        """ Return len(self). """
        pass


