# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QPaintEvent(__PyQt5_QtCore.QEvent):
    """
    QPaintEvent(QRegion)
    QPaintEvent(QRect)
    QPaintEvent(QPaintEvent)
    """
    def rect(self): # real signature unknown; restored from __doc__
        """ rect(self) -> QRect """
        pass

    def region(self): # real signature unknown; restored from __doc__
        """ region(self) -> QRegion """
        return QRegion

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


