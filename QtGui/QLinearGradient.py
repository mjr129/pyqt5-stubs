# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QGradient import QGradient

class QLinearGradient(QGradient):
    """
    QLinearGradient()
    QLinearGradient(Union[QPointF, QPoint], Union[QPointF, QPoint])
    QLinearGradient(float, float, float, float)
    QLinearGradient(QLinearGradient)
    """
    def finalStop(self): # real signature unknown; restored from __doc__
        """ finalStop(self) -> QPointF """
        pass

    def setFinalStop(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setFinalStop(self, Union[QPointF, QPoint])
        setFinalStop(self, float, float)
        """
        pass

    def setStart(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        setStart(self, Union[QPointF, QPoint])
        setStart(self, float, float)
        """
        pass

    def start(self): # real signature unknown; restored from __doc__
        """ start(self) -> QPointF """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


