# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QOpenGLContextGroup(__PyQt5_QtCore.QObject):
    # no doc
    def currentContextGroup(self): # real signature unknown; restored from __doc__
        """ currentContextGroup() -> QOpenGLContextGroup """
        return QOpenGLContextGroup

    def shares(self): # real signature unknown; restored from __doc__
        """ shares(self) -> object """
        return object()

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


