# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QStatusTipEvent(__PyQt5_QtCore.QEvent):
    """
    QStatusTipEvent(str)
    QStatusTipEvent(QStatusTipEvent)
    """
    def tip(self): # real signature unknown; restored from __doc__
        """ tip(self) -> str """
        return ""

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


