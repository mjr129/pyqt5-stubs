# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QActionEvent(__PyQt5_QtCore.QEvent):
    """
    QActionEvent(int, QAction, before: QAction = None)
    QActionEvent(QActionEvent)
    """
    def action(self): # real signature unknown; restored from __doc__
        """ action(self) -> QAction """
        pass

    def before(self): # real signature unknown; restored from __doc__
        """ before(self) -> QAction """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


