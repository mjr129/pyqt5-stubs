# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPaintDeviceWindow import QPaintDeviceWindow

class QRasterWindow(QPaintDeviceWindow):
    """ QRasterWindow(parent: QWindow = None) """
    def metric(self, QPaintDevice_PaintDeviceMetric): # real signature unknown; restored from __doc__
        """ metric(self, QPaintDevice.PaintDeviceMetric) -> int """
        return 0

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


