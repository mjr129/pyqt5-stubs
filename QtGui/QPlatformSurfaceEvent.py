# encoding: utf-8
# module PyQt5.QtGui
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtGui.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QPlatformSurfaceEvent(__PyQt5_QtCore.QEvent):
    """
    QPlatformSurfaceEvent(QPlatformSurfaceEvent.SurfaceEventType)
    QPlatformSurfaceEvent(QPlatformSurfaceEvent)
    """
    def surfaceEventType(self): # real signature unknown; restored from __doc__
        """ surfaceEventType(self) -> QPlatformSurfaceEvent.SurfaceEventType """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    SurfaceAboutToBeDestroyed = 1
    SurfaceCreated = 0


