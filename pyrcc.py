# encoding: utf-8
# module PyQt5.pyrcc
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/pyrcc.so
# by generator 1.145
# no doc
# no imports

# Variables with simple values

CONSTANT_COMPRESSLEVEL_DEFAULT = -1

CONSTANT_COMPRESSTHRESHOLD_DEFAULT = 70

# no functions
# classes

class RCCResourceLibrary(): # skipped bases: <class 'sip.simplewrapper'>
    # no doc
    def dataFiles(self, *args, **kwargs): # real signature unknown
        pass

    def output(self, *args, **kwargs): # real signature unknown
        pass

    def readFiles(self, *args, **kwargs): # real signature unknown
        pass

    def setCompressLevel(self, *args, **kwargs): # real signature unknown
        pass

    def setCompressThreshold(self, *args, **kwargs): # real signature unknown
        pass

    def setInitName(self, *args, **kwargs): # real signature unknown
        pass

    def setInputFiles(self, *args, **kwargs): # real signature unknown
        pass

    def setResourceRoot(self, *args, **kwargs): # real signature unknown
        pass

    def setVerbose(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values



