# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

from .QDomNode import QDomNode

class QDomDocumentType(QDomNode):
    """
    QDomDocumentType()
    QDomDocumentType(QDomDocumentType)
    """
    def entities(self): # real signature unknown; restored from __doc__
        """ entities(self) -> QDomNamedNodeMap """
        return QDomNamedNodeMap

    def internalSubset(self): # real signature unknown; restored from __doc__
        """ internalSubset(self) -> str """
        return ""

    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def nodeType(self): # real signature unknown; restored from __doc__
        """ nodeType(self) -> QDomNode.NodeType """
        pass

    def notations(self): # real signature unknown; restored from __doc__
        """ notations(self) -> QDomNamedNodeMap """
        return QDomNamedNodeMap

    def publicId(self): # real signature unknown; restored from __doc__
        """ publicId(self) -> str """
        return ""

    def systemId(self): # real signature unknown; restored from __doc__
        """ systemId(self) -> str """
        return ""

    def __init__(self, QDomDocumentType=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


