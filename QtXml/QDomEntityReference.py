# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

from .QDomNode import QDomNode

class QDomEntityReference(QDomNode):
    """
    QDomEntityReference()
    QDomEntityReference(QDomEntityReference)
    """
    def nodeType(self): # real signature unknown; restored from __doc__
        """ nodeType(self) -> QDomNode.NodeType """
        pass

    def __init__(self, QDomEntityReference=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


