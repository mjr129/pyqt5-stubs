# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

class QXmlLexicalHandler(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QXmlLexicalHandler()
    QXmlLexicalHandler(QXmlLexicalHandler)
    """
    def comment(self, p_str): # real signature unknown; restored from __doc__
        """ comment(self, str) -> bool """
        return False

    def endCDATA(self): # real signature unknown; restored from __doc__
        """ endCDATA(self) -> bool """
        return False

    def endDTD(self): # real signature unknown; restored from __doc__
        """ endDTD(self) -> bool """
        return False

    def endEntity(self, p_str): # real signature unknown; restored from __doc__
        """ endEntity(self, str) -> bool """
        return False

    def errorString(self): # real signature unknown; restored from __doc__
        """ errorString(self) -> str """
        return ""

    def startCDATA(self): # real signature unknown; restored from __doc__
        """ startCDATA(self) -> bool """
        return False

    def startDTD(self, p_str, p_str_1, p_str_2): # real signature unknown; restored from __doc__
        """ startDTD(self, str, str, str) -> bool """
        return False

    def startEntity(self, p_str): # real signature unknown; restored from __doc__
        """ startEntity(self, str) -> bool """
        return False

    def __init__(self, QXmlLexicalHandler=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



