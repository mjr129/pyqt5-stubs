# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

class QXmlEntityResolver(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QXmlEntityResolver()
    QXmlEntityResolver(QXmlEntityResolver)
    """
    def errorString(self): # real signature unknown; restored from __doc__
        """ errorString(self) -> str """
        return ""

    def resolveEntity(self, p_str, p_str_1): # real signature unknown; restored from __doc__
        """ resolveEntity(self, str, str) -> Tuple[bool, QXmlInputSource] """
        pass

    def __init__(self, QXmlEntityResolver=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



