# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

from .QDomNode import QDomNode

class QDomAttr(QDomNode):
    """
    QDomAttr()
    QDomAttr(QDomAttr)
    """
    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def nodeType(self): # real signature unknown; restored from __doc__
        """ nodeType(self) -> QDomNode.NodeType """
        pass

    def ownerElement(self): # real signature unknown; restored from __doc__
        """ ownerElement(self) -> QDomElement """
        return QDomElement

    def setValue(self, p_str): # real signature unknown; restored from __doc__
        """ setValue(self, str) """
        pass

    def specified(self): # real signature unknown; restored from __doc__
        """ specified(self) -> bool """
        return False

    def value(self): # real signature unknown; restored from __doc__
        """ value(self) -> str """
        return ""

    def __init__(self, QDomAttr=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


