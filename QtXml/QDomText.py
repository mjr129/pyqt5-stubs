# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

from .QDomCharacterData import QDomCharacterData

class QDomText(QDomCharacterData):
    """
    QDomText()
    QDomText(QDomText)
    """
    def nodeType(self): # real signature unknown; restored from __doc__
        """ nodeType(self) -> QDomNode.NodeType """
        pass

    def splitText(self, p_int): # real signature unknown; restored from __doc__
        """ splitText(self, int) -> QDomText """
        return QDomText

    def __init__(self, QDomText=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


