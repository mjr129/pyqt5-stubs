# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

class QXmlParseException(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QXmlParseException(name: str = '', column: int = -1, line: int = -1, publicId: str = '', systemId: str = '')
    QXmlParseException(QXmlParseException)
    """
    def columnNumber(self): # real signature unknown; restored from __doc__
        """ columnNumber(self) -> int """
        return 0

    def lineNumber(self): # real signature unknown; restored from __doc__
        """ lineNumber(self) -> int """
        return 0

    def message(self): # real signature unknown; restored from __doc__
        """ message(self) -> str """
        return ""

    def publicId(self): # real signature unknown; restored from __doc__
        """ publicId(self) -> str """
        return ""

    def systemId(self): # real signature unknown; restored from __doc__
        """ systemId(self) -> str """
        return ""

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



