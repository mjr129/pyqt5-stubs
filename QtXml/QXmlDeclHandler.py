# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

class QXmlDeclHandler(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QXmlDeclHandler()
    QXmlDeclHandler(QXmlDeclHandler)
    """
    def attributeDecl(self, p_str, p_str_1, p_str_2, p_str_3, p_str_4): # real signature unknown; restored from __doc__
        """ attributeDecl(self, str, str, str, str, str) -> bool """
        return False

    def errorString(self): # real signature unknown; restored from __doc__
        """ errorString(self) -> str """
        return ""

    def externalEntityDecl(self, p_str, p_str_1, p_str_2): # real signature unknown; restored from __doc__
        """ externalEntityDecl(self, str, str, str) -> bool """
        return False

    def internalEntityDecl(self, p_str, p_str_1): # real signature unknown; restored from __doc__
        """ internalEntityDecl(self, str, str) -> bool """
        return False

    def __init__(self, QXmlDeclHandler=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



