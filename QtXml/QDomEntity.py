# encoding: utf-8
# module PyQt5.QtXml
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtXml.so
# by generator 1.145
# no doc
# no imports

from .QDomNode import QDomNode

class QDomEntity(QDomNode):
    """
    QDomEntity()
    QDomEntity(QDomEntity)
    """
    def nodeType(self): # real signature unknown; restored from __doc__
        """ nodeType(self) -> QDomNode.NodeType """
        pass

    def notationName(self): # real signature unknown; restored from __doc__
        """ notationName(self) -> str """
        return ""

    def publicId(self): # real signature unknown; restored from __doc__
        """ publicId(self) -> str """
        return ""

    def systemId(self): # real signature unknown; restored from __doc__
        """ systemId(self) -> str """
        return ""

    def __init__(self, QDomEntity=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


