# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QAbstractNativeEventFilter(): # skipped bases: <class 'sip.simplewrapper'>
    """ QAbstractNativeEventFilter() """
    def nativeEventFilter(self, Union, QByteArray=None, bytes=None, bytearray=None, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """ nativeEventFilter(self, Union[QByteArray, bytes, bytearray], sip.voidptr) -> Tuple[bool, int] """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



