# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QMutexLocker(): # skipped bases: <class 'sip.simplewrapper'>
    """ QMutexLocker(QMutex) """
    def mutex(self): # real signature unknown; restored from __doc__
        """ mutex(self) -> QMutex """
        return QMutex

    def relock(self): # real signature unknown; restored from __doc__
        """ relock(self) """
        pass

    def unlock(self): # real signature unknown; restored from __doc__
        """ unlock(self) """
        pass

    def __enter__(self): # real signature unknown; restored from __doc__
        """ __enter__(self) -> object """
        return object()

    def __exit__(self, p_object, p_object_1, p_object_2): # real signature unknown; restored from __doc__
        """ __exit__(self, object, object, object) """
        pass

    def __init__(self, QMutex): # real signature unknown; restored from __doc__
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



