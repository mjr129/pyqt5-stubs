# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

from .QEvent import QEvent

class QDynamicPropertyChangeEvent(QEvent):
    """
    QDynamicPropertyChangeEvent(Union[QByteArray, bytes, bytearray])
    QDynamicPropertyChangeEvent(QDynamicPropertyChangeEvent)
    """
    def propertyName(self): # real signature unknown; restored from __doc__
        """ propertyName(self) -> QByteArray """
        return QByteArray

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


