# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QMessageLogger(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QMessageLogger()
    QMessageLogger(str, int, str)
    QMessageLogger(str, int, str, str)
    """
    def critical(self, p_str): # real signature unknown; restored from __doc__
        """ critical(self, str) """
        pass

    def debug(self, p_str): # real signature unknown; restored from __doc__
        """ debug(self, str) """
        pass

    def fatal(self, p_str): # real signature unknown; restored from __doc__
        """ fatal(self, str) """
        pass

    def info(self, p_str): # real signature unknown; restored from __doc__
        """ info(self, str) """
        pass

    def warning(self, p_str): # real signature unknown; restored from __doc__
        """ warning(self, str) """
        pass

    def __init__(self, p_str=None, p_int=None, p_str_1=None, p_str_2=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



