# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QBasicTimer(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QBasicTimer()
    QBasicTimer(QBasicTimer)
    """
    def isActive(self): # real signature unknown; restored from __doc__
        """ isActive(self) -> bool """
        return False

    def start(self, p_int, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        start(self, int, Qt.TimerType, QObject)
        start(self, int, QObject)
        """
        pass

    def stop(self): # real signature unknown; restored from __doc__
        """ stop(self) """
        pass

    def timerId(self): # real signature unknown; restored from __doc__
        """ timerId(self) -> int """
        return 0

    def __init__(self, QBasicTimer=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



