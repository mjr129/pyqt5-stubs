# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

from .QObject import QObject

class QObjectCleanupHandler(QObject):
    """ QObjectCleanupHandler() """
    def add(self, QObject): # real signature unknown; restored from __doc__
        """ add(self, QObject) -> QObject """
        return QObject

    def clear(self): # real signature unknown; restored from __doc__
        """ clear(self) """
        pass

    def isEmpty(self): # real signature unknown; restored from __doc__
        """ isEmpty(self) -> bool """
        return False

    def remove(self, QObject): # real signature unknown; restored from __doc__
        """ remove(self, QObject) """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass


