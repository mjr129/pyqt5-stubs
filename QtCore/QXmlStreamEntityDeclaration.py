# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QXmlStreamEntityDeclaration(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QXmlStreamEntityDeclaration()
    QXmlStreamEntityDeclaration(QXmlStreamEntityDeclaration)
    """
    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def notationName(self): # real signature unknown; restored from __doc__
        """ notationName(self) -> str """
        return ""

    def publicId(self): # real signature unknown; restored from __doc__
        """ publicId(self) -> str """
        return ""

    def systemId(self): # real signature unknown; restored from __doc__
        """ systemId(self) -> str """
        return ""

    def value(self): # real signature unknown; restored from __doc__
        """ value(self) -> str """
        return ""

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QXmlStreamEntityDeclaration=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


