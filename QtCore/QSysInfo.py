# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QSysInfo(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QSysInfo()
    QSysInfo(QSysInfo)
    """
    def buildAbi(self): # real signature unknown; restored from __doc__
        """ buildAbi() -> str """
        return ""

    def buildCpuArchitecture(self): # real signature unknown; restored from __doc__
        """ buildCpuArchitecture() -> str """
        return ""

    def currentCpuArchitecture(self): # real signature unknown; restored from __doc__
        """ currentCpuArchitecture() -> str """
        return ""

    def kernelType(self): # real signature unknown; restored from __doc__
        """ kernelType() -> str """
        return ""

    def kernelVersion(self): # real signature unknown; restored from __doc__
        """ kernelVersion() -> str """
        return ""

    def machineHostName(self): # real signature unknown; restored from __doc__
        """ machineHostName() -> str """
        return ""

    def macVersion(self): # real signature unknown; restored from __doc__
        """ macVersion() -> QSysInfo.MacVersion """
        pass

    def prettyProductName(self): # real signature unknown; restored from __doc__
        """ prettyProductName() -> str """
        return ""

    def productType(self): # real signature unknown; restored from __doc__
        """ productType() -> str """
        return ""

    def productVersion(self): # real signature unknown; restored from __doc__
        """ productVersion() -> str """
        return ""

    def __init__(self, QSysInfo=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    BigEndian = 0
    ByteOrder = 1
    LittleEndian = 1
    MacintoshVersion = 14
    MV_10_0 = 2
    MV_10_1 = 3
    MV_10_10 = 12
    MV_10_11 = 13
    MV_10_12 = 14
    MV_10_2 = 4
    MV_10_3 = 5
    MV_10_4 = 6
    MV_10_5 = 7
    MV_10_6 = 8
    MV_10_7 = 9
    MV_10_8 = 10
    MV_10_9 = 11
    MV_9 = 1
    MV_CHEETAH = 2
    MV_ELCAPITAN = 13
    MV_IOS = 256
    MV_IOS_10_0 = 416
    MV_IOS_4_3 = 323
    MV_IOS_5_0 = 336
    MV_IOS_5_1 = 337
    MV_IOS_6_0 = 352
    MV_IOS_6_1 = 353
    MV_IOS_7_0 = 368
    MV_IOS_7_1 = 369
    MV_IOS_8_0 = 384
    MV_IOS_8_1 = 385
    MV_IOS_8_2 = 386
    MV_IOS_8_3 = 387
    MV_IOS_8_4 = 388
    MV_IOS_9_0 = 400
    MV_IOS_9_1 = 401
    MV_IOS_9_2 = 402
    MV_IOS_9_3 = 403
    MV_JAGUAR = 4
    MV_LEOPARD = 7
    MV_LION = 9
    MV_MAVERICKS = 11
    MV_MOUNTAINLION = 10
    MV_PANTHER = 5
    MV_PUMA = 3
    MV_SIERRA = 14
    MV_SNOWLEOPARD = 8
    MV_TIGER = 6
    MV_TVOS = 512
    MV_TVOS_10_0 = 672
    MV_TVOS_9_0 = 656
    MV_TVOS_9_1 = 657
    MV_TVOS_9_2 = 658
    MV_Unknown = 0
    MV_WATCHOS = 1024
    MV_WATCHOS_2_0 = 1056
    MV_WATCHOS_2_1 = 1057
    MV_WATCHOS_2_2 = 1058
    MV_WATCHOS_3_0 = 1072
    MV_YOSEMITE = 12
    WordSize = 64


