# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QTextEncoder(): # skipped bases: <class 'sip.wrapper'>
    """
    QTextEncoder(QTextCodec)
    QTextEncoder(QTextCodec, Union[QTextCodec.ConversionFlags, QTextCodec.ConversionFlag])
    """
    def fromUnicode(self, p_str): # real signature unknown; restored from __doc__
        """ fromUnicode(self, str) -> QByteArray """
        return QByteArray

    def __init__(self, QTextCodec, Union=None, QTextCodec_ConversionFlags=None, QTextCodec_ConversionFlag=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



