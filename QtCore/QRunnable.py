# encoding: utf-8
# module PyQt5.QtCore
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtCore.so
# by generator 1.145
# no doc
# no imports

class QRunnable(): # skipped bases: <class 'sip.wrapper'>
    """
    QRunnable()
    QRunnable(QRunnable)
    """
    def autoDelete(self): # real signature unknown; restored from __doc__
        """ autoDelete(self) -> bool """
        return False

    def run(self): # real signature unknown; restored from __doc__
        """ run(self) """
        pass

    def setAutoDelete(self, bool): # real signature unknown; restored from __doc__
        """ setAutoDelete(self, bool) """
        pass

    def __init__(self, QRunnable=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



