# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceReply import QPlaceReply

class QPlaceSearchSuggestionReply(QPlaceReply):
    """ QPlaceSearchSuggestionReply(parent: QObject = None) """
    def setSuggestions(self, Iterable, p_str=None): # real signature unknown; restored from __doc__
        """ setSuggestions(self, Iterable[str]) """
        pass

    def suggestions(self): # real signature unknown; restored from __doc__
        """ suggestions(self) -> List[str] """
        return []

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> QPlaceReply.Type """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


