# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QPlaceMatchRequest(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QPlaceMatchRequest()
    QPlaceMatchRequest(QPlaceMatchRequest)
    """
    def clear(self): # real signature unknown; restored from __doc__
        """ clear(self) """
        pass

    def parameters(self): # real signature unknown; restored from __doc__
        """ parameters(self) -> Dict[str, Any] """
        return {}

    def places(self): # real signature unknown; restored from __doc__
        """ places(self) -> List[QPlace] """
        return []

    def setParameters(self, Dict, p_str=None, Any=None): # real signature unknown; restored from __doc__
        """ setParameters(self, Dict[str, Any]) """
        pass

    def setPlaces(self, Iterable, QPlace=None): # real signature unknown; restored from __doc__
        """ setPlaces(self, Iterable[QPlace]) """
        pass

    def setResults(self, p_object): # real signature unknown; restored from __doc__
        """ setResults(self, object) """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QPlaceMatchRequest=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    AlternativeId = 'alternativeId'
    __hash__ = None


