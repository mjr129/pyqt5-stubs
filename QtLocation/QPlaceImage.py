# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceContent import QPlaceContent

class QPlaceImage(QPlaceContent):
    """
    QPlaceImage()
    QPlaceImage(QPlaceContent)
    QPlaceImage(QPlaceImage)
    """
    def imageId(self): # real signature unknown; restored from __doc__
        """ imageId(self) -> str """
        return ""

    def mimeType(self): # real signature unknown; restored from __doc__
        """ mimeType(self) -> str """
        return ""

    def setImageId(self, p_str): # real signature unknown; restored from __doc__
        """ setImageId(self, str) """
        pass

    def setMimeType(self, p_str): # real signature unknown; restored from __doc__
        """ setMimeType(self, str) """
        pass

    def setUrl(self, QUrl): # real signature unknown; restored from __doc__
        """ setUrl(self, QUrl) """
        pass

    def url(self): # real signature unknown; restored from __doc__
        """ url(self) -> QUrl """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


