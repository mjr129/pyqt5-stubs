# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceReply import QPlaceReply

class QPlaceDetailsReply(QPlaceReply):
    """ QPlaceDetailsReply(parent: QObject = None) """
    def place(self): # real signature unknown; restored from __doc__
        """ place(self) -> QPlace """
        return QPlace

    def setPlace(self, QPlace): # real signature unknown; restored from __doc__
        """ setPlace(self, QPlace) """
        pass

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> QPlaceReply.Type """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


