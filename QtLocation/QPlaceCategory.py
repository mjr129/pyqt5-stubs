# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QPlaceCategory(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QPlaceCategory()
    QPlaceCategory(QPlaceCategory)
    """
    def categoryId(self): # real signature unknown; restored from __doc__
        """ categoryId(self) -> str """
        return ""

    def icon(self): # real signature unknown; restored from __doc__
        """ icon(self) -> QPlaceIcon """
        return QPlaceIcon

    def isEmpty(self): # real signature unknown; restored from __doc__
        """ isEmpty(self) -> bool """
        return False

    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def setCategoryId(self, p_str): # real signature unknown; restored from __doc__
        """ setCategoryId(self, str) """
        pass

    def setIcon(self, QPlaceIcon): # real signature unknown; restored from __doc__
        """ setIcon(self, QPlaceIcon) """
        pass

    def setName(self, p_str): # real signature unknown; restored from __doc__
        """ setName(self, str) """
        pass

    def setVisibility(self, QLocation_Visibility): # real signature unknown; restored from __doc__
        """ setVisibility(self, QLocation.Visibility) """
        pass

    def visibility(self): # real signature unknown; restored from __doc__
        """ visibility(self) -> QLocation.Visibility """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QPlaceCategory=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


