# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceReply import QPlaceReply

class QPlaceMatchReply(QPlaceReply):
    """ QPlaceMatchReply(parent: QObject = None) """
    def places(self): # real signature unknown; restored from __doc__
        """ places(self) -> object """
        return object()

    def request(self): # real signature unknown; restored from __doc__
        """ request(self) -> QPlaceMatchRequest """
        return QPlaceMatchRequest

    def setPlaces(self, Iterable, QPlace=None): # real signature unknown; restored from __doc__
        """ setPlaces(self, Iterable[QPlace]) """
        pass

    def setRequest(self, QPlaceMatchRequest): # real signature unknown; restored from __doc__
        """ setRequest(self, QPlaceMatchRequest) """
        pass

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> QPlaceReply.Type """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


