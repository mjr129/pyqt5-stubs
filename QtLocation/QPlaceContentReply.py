# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceReply import QPlaceReply

class QPlaceContentReply(QPlaceReply):
    """ QPlaceContentReply(parent: QObject = None) """
    def content(self): # real signature unknown; restored from __doc__
        """ content(self) -> object """
        return object()

    def nextPageRequest(self): # real signature unknown; restored from __doc__
        """ nextPageRequest(self) -> QPlaceContentRequest """
        return QPlaceContentRequest

    def previousPageRequest(self): # real signature unknown; restored from __doc__
        """ previousPageRequest(self) -> QPlaceContentRequest """
        return QPlaceContentRequest

    def request(self): # real signature unknown; restored from __doc__
        """ request(self) -> QPlaceContentRequest """
        return QPlaceContentRequest

    def setContent(self, p_object): # real signature unknown; restored from __doc__
        """ setContent(self, object) """
        pass

    def setNextPageRequest(self, QPlaceContentRequest): # real signature unknown; restored from __doc__
        """ setNextPageRequest(self, QPlaceContentRequest) """
        pass

    def setPreviousPageRequest(self, QPlaceContentRequest): # real signature unknown; restored from __doc__
        """ setPreviousPageRequest(self, QPlaceContentRequest) """
        pass

    def setRequest(self, QPlaceContentRequest): # real signature unknown; restored from __doc__
        """ setRequest(self, QPlaceContentRequest) """
        pass

    def setTotalCount(self, p_int): # real signature unknown; restored from __doc__
        """ setTotalCount(self, int) """
        pass

    def totalCount(self): # real signature unknown; restored from __doc__
        """ totalCount(self) -> int """
        return 0

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> QPlaceReply.Type """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


