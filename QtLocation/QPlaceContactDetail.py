# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


class QPlaceContactDetail(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QPlaceContactDetail()
    QPlaceContactDetail(QPlaceContactDetail)
    """
    def clear(self): # real signature unknown; restored from __doc__
        """ clear(self) """
        pass

    def label(self): # real signature unknown; restored from __doc__
        """ label(self) -> str """
        return ""

    def setLabel(self, p_str): # real signature unknown; restored from __doc__
        """ setLabel(self, str) """
        pass

    def setValue(self, p_str): # real signature unknown; restored from __doc__
        """ setValue(self, str) """
        pass

    def value(self): # real signature unknown; restored from __doc__
        """ value(self) -> str """
        return ""

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QPlaceContactDetail=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    Email = 'email'
    Fax = 'fax'
    Phone = 'phone'
    Website = 'website'
    __hash__ = None


