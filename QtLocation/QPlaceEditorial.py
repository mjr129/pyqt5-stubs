# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceContent import QPlaceContent

class QPlaceEditorial(QPlaceContent):
    """
    QPlaceEditorial()
    QPlaceEditorial(QPlaceContent)
    QPlaceEditorial(QPlaceEditorial)
    """
    def language(self): # real signature unknown; restored from __doc__
        """ language(self) -> str """
        return ""

    def setLanguage(self, p_str): # real signature unknown; restored from __doc__
        """ setLanguage(self, str) """
        pass

    def setText(self, p_str): # real signature unknown; restored from __doc__
        """ setText(self, str) """
        pass

    def setTitle(self, p_str): # real signature unknown; restored from __doc__
        """ setTitle(self, str) """
        pass

    def text(self): # real signature unknown; restored from __doc__
        """ text(self) -> str """
        return ""

    def title(self): # real signature unknown; restored from __doc__
        """ title(self) -> str """
        return ""

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


