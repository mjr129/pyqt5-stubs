# encoding: utf-8
# module PyQt5.QtLocation
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtLocation.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore


from .QPlaceSearchResult import QPlaceSearchResult

class QPlaceProposedSearchResult(QPlaceSearchResult):
    """
    QPlaceProposedSearchResult()
    QPlaceProposedSearchResult(QPlaceSearchResult)
    QPlaceProposedSearchResult(QPlaceProposedSearchResult)
    """
    def searchRequest(self): # real signature unknown; restored from __doc__
        """ searchRequest(self) -> QPlaceSearchRequest """
        return QPlaceSearchRequest

    def setSearchRequest(self, QPlaceSearchRequest): # real signature unknown; restored from __doc__
        """ setSearchRequest(self, QPlaceSearchRequest) """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


