# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


class QItemEditorCreatorBase(): # skipped bases: <class 'sip.wrapper'>
    """
    QItemEditorCreatorBase()
    QItemEditorCreatorBase(QItemEditorCreatorBase)
    """
    def createWidget(self, QWidget): # real signature unknown; restored from __doc__
        """ createWidget(self, QWidget) -> QWidget """
        return QWidget

    def valuePropertyName(self): # real signature unknown; restored from __doc__
        """ valuePropertyName(self) -> QByteArray """
        pass

    def __init__(self, QItemEditorCreatorBase=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



