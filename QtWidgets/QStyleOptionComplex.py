# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionComplex(QStyleOption):
    """
    QStyleOptionComplex(version: int = QStyleOptionComplex.Version, type: int = QStyleOption.SO_Complex)
    QStyleOptionComplex(QStyleOptionComplex)
    """
    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    Type = 983040
    Version = 1


