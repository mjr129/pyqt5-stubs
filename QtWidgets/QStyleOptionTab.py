# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionTab(QStyleOption):
    """
    QStyleOptionTab()
    QStyleOptionTab(QStyleOptionTab)
    """
    def __init__(self, QStyleOptionTab=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    Beginning = 0
    End = 2
    HasFrame = 1
    LeftCornerWidget = 1
    Middle = 1
    NextIsSelected = 1
    NoCornerWidgets = 0
    None_ = 0
    NotAdjacent = 0
    OnlyOneTab = 3
    PreviousIsSelected = 2
    RightCornerWidget = 2
    Type = 3
    Version = 3


