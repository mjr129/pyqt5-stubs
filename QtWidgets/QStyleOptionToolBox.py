# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionToolBox(QStyleOption):
    """
    QStyleOptionToolBox()
    QStyleOptionToolBox(QStyleOptionToolBox)
    """
    def __init__(self, QStyleOptionToolBox=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    Beginning = 0
    End = 2
    Middle = 1
    NextIsSelected = 1
    NotAdjacent = 0
    OnlyOneTab = 3
    PreviousIsSelected = 2
    Type = 7
    Version = 2


