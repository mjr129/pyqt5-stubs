# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QAbstractSlider import QAbstractSlider

class QDial(QAbstractSlider):
    """ QDial(parent: QWidget = None) """
    def event(self, QEvent): # real signature unknown; restored from __doc__
        """ event(self, QEvent) -> bool """
        return False

    def initStyleOption(self, QStyleOptionSlider): # real signature unknown; restored from __doc__
        """ initStyleOption(self, QStyleOptionSlider) """
        pass

    def minimumSizeHint(self): # real signature unknown; restored from __doc__
        """ minimumSizeHint(self) -> QSize """
        pass

    def mouseMoveEvent(self, QMouseEvent): # real signature unknown; restored from __doc__
        """ mouseMoveEvent(self, QMouseEvent) """
        pass

    def mousePressEvent(self, QMouseEvent): # real signature unknown; restored from __doc__
        """ mousePressEvent(self, QMouseEvent) """
        pass

    def mouseReleaseEvent(self, QMouseEvent): # real signature unknown; restored from __doc__
        """ mouseReleaseEvent(self, QMouseEvent) """
        pass

    def notchesVisible(self): # real signature unknown; restored from __doc__
        """ notchesVisible(self) -> bool """
        return False

    def notchSize(self): # real signature unknown; restored from __doc__
        """ notchSize(self) -> int """
        return 0

    def notchTarget(self): # real signature unknown; restored from __doc__
        """ notchTarget(self) -> float """
        return 0.0

    def paintEvent(self, QPaintEvent): # real signature unknown; restored from __doc__
        """ paintEvent(self, QPaintEvent) """
        pass

    def resizeEvent(self, QResizeEvent): # real signature unknown; restored from __doc__
        """ resizeEvent(self, QResizeEvent) """
        pass

    def setNotchesVisible(self, bool): # real signature unknown; restored from __doc__
        """ setNotchesVisible(self, bool) """
        pass

    def setNotchTarget(self, p_float): # real signature unknown; restored from __doc__
        """ setNotchTarget(self, float) """
        pass

    def setWrapping(self, bool): # real signature unknown; restored from __doc__
        """ setWrapping(self, bool) """
        pass

    def sizeHint(self): # real signature unknown; restored from __doc__
        """ sizeHint(self) -> QSize """
        pass

    def sliderChange(self, QAbstractSlider_SliderChange): # real signature unknown; restored from __doc__
        """ sliderChange(self, QAbstractSlider.SliderChange) """
        pass

    def wrapping(self): # real signature unknown; restored from __doc__
        """ wrapping(self) -> bool """
        return False

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


