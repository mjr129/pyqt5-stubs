# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QGraphicsSceneEvent import QGraphicsSceneEvent

class QGraphicsSceneResizeEvent(QGraphicsSceneEvent):
    """ QGraphicsSceneResizeEvent() """
    def newSize(self): # real signature unknown; restored from __doc__
        """ newSize(self) -> QSizeF """
        pass

    def oldSize(self): # real signature unknown; restored from __doc__
        """ oldSize(self) -> QSizeF """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass


