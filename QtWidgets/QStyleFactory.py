# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


class QStyleFactory(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QStyleFactory()
    QStyleFactory(QStyleFactory)
    """
    def create(self, p_str): # real signature unknown; restored from __doc__
        """ create(str) -> QStyle """
        return QStyle

    def keys(self): # real signature unknown; restored from __doc__
        """ keys() -> List[str] """
        return []

    def __init__(self, QStyleFactory=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



