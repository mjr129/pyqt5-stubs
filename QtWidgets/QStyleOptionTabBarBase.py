# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionTabBarBase(QStyleOption):
    """
    QStyleOptionTabBarBase()
    QStyleOptionTabBarBase(QStyleOptionTabBarBase)
    """
    def __init__(self, QStyleOptionTabBarBase=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    Type = 12
    Version = 2


