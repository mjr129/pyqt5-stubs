# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionMenuItem(QStyleOption):
    """
    QStyleOptionMenuItem()
    QStyleOptionMenuItem(QStyleOptionMenuItem)
    """
    def __init__(self, QStyleOptionMenuItem=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    DefaultItem = 1
    EmptyArea = 7
    Exclusive = 1
    Margin = 6
    NonExclusive = 2
    Normal = 0
    NotCheckable = 0
    Scroller = 4
    Separator = 2
    SubMenu = 3
    TearOff = 5
    Type = 4
    Version = 1


