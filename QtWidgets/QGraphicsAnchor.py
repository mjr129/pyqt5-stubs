# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


class QGraphicsAnchor(__PyQt5_QtCore.QObject):
    # no doc
    def setSizePolicy(self, QSizePolicy_Policy): # real signature unknown; restored from __doc__
        """ setSizePolicy(self, QSizePolicy.Policy) """
        pass

    def setSpacing(self, p_float): # real signature unknown; restored from __doc__
        """ setSpacing(self, float) """
        pass

    def sizePolicy(self): # real signature unknown; restored from __doc__
        """ sizePolicy(self) -> QSizePolicy.Policy """
        pass

    def spacing(self): # real signature unknown; restored from __doc__
        """ spacing(self) -> float """
        return 0.0

    def unsetSpacing(self): # real signature unknown; restored from __doc__
        """ unsetSpacing(self) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


