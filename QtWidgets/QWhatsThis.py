# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


class QWhatsThis(): # skipped bases: <class 'sip.simplewrapper'>
    """ QWhatsThis(QWhatsThis) """
    def createAction(self, parent=None): # real signature unknown; restored from __doc__
        """ createAction(parent: QObject = None) -> QAction """
        return QAction

    def enterWhatsThisMode(self): # real signature unknown; restored from __doc__
        """ enterWhatsThisMode() """
        pass

    def hideText(self): # real signature unknown; restored from __doc__
        """ hideText() """
        pass

    def inWhatsThisMode(self): # real signature unknown; restored from __doc__
        """ inWhatsThisMode() -> bool """
        return False

    def leaveWhatsThisMode(self): # real signature unknown; restored from __doc__
        """ leaveWhatsThisMode() """
        pass

    def showText(self, QPoint, p_str, widget=None): # real signature unknown; restored from __doc__
        """ showText(QPoint, str, widget: QWidget = None) """
        pass

    def __init__(self, QWhatsThis): # real signature unknown; restored from __doc__
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



