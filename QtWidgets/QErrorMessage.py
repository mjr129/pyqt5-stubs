# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QDialog import QDialog

class QErrorMessage(QDialog):
    """ QErrorMessage(parent: QWidget = None) """
    def changeEvent(self, QEvent): # real signature unknown; restored from __doc__
        """ changeEvent(self, QEvent) """
        pass

    def done(self, p_int): # real signature unknown; restored from __doc__
        """ done(self, int) """
        pass

    def qtHandler(self): # real signature unknown; restored from __doc__
        """ qtHandler() -> QErrorMessage """
        return QErrorMessage

    def showMessage(self, p_str, p_str_1=None): # real signature unknown; restored from __doc__ with multiple overloads
        """
        showMessage(self, str)
        showMessage(self, str, str)
        """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


