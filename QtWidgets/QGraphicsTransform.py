# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


class QGraphicsTransform(__PyQt5_QtCore.QObject):
    """ QGraphicsTransform(parent: QObject = None) """
    def applyTo(self, QMatrix4x4): # real signature unknown; restored from __doc__
        """ applyTo(self, QMatrix4x4) """
        pass

    def update(self): # real signature unknown; restored from __doc__
        """ update(self) """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


