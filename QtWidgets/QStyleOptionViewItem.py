# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionViewItem(QStyleOption):
    """
    QStyleOptionViewItem()
    QStyleOptionViewItem(QStyleOptionViewItem)
    """
    def __init__(self, QStyleOptionViewItem=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    Alternate = 2
    Beginning = 1
    Bottom = 3
    End = 3
    HasCheckIndicator = 4
    HasDecoration = 16
    HasDisplay = 8
    Invalid = 0
    Left = 0
    Middle = 2
    None_ = 0
    OnlyOne = 4
    Right = 1
    Top = 2
    Type = 10
    Version = 4
    WrapText = 1


