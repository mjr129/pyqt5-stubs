# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QWidget import QWidget

class QMacCocoaViewContainer(QWidget):
    """ QMacCocoaViewContainer(sip.voidptr, parent: QWidget = None) """
    def cocoaView(self): # real signature unknown; restored from __doc__
        """ cocoaView(self) -> sip.voidptr """
        pass

    def setCocoaView(self, sip_voidptr): # real signature unknown; restored from __doc__
        """ setCocoaView(self, sip.voidptr) """
        pass

    def __init__(self, sip_voidptr, parent=None): # real signature unknown; restored from __doc__
        pass


