# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QStyleOption import QStyleOption

class QStyleOptionToolBar(QStyleOption):
    """
    QStyleOptionToolBar()
    QStyleOptionToolBar(QStyleOptionToolBar)
    """
    def __init__(self, QStyleOptionToolBar=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    Beginning = 0
    End = 2
    Middle = 1
    Movable = 1
    None_ = 0
    OnlyOne = 3
    Type = 14
    Version = 1


