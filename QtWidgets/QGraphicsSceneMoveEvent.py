# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QGraphicsSceneEvent import QGraphicsSceneEvent

class QGraphicsSceneMoveEvent(QGraphicsSceneEvent):
    """ QGraphicsSceneMoveEvent() """
    def newPos(self): # real signature unknown; restored from __doc__
        """ newPos(self) -> QPointF """
        pass

    def oldPos(self): # real signature unknown; restored from __doc__
        """ oldPos(self) -> QPointF """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass


