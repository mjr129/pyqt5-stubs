# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QGraphicsTransform import QGraphicsTransform

class QGraphicsScale(QGraphicsTransform):
    """ QGraphicsScale(parent: QObject = None) """
    def applyTo(self, QMatrix4x4): # real signature unknown; restored from __doc__
        """ applyTo(self, QMatrix4x4) """
        pass

    def origin(self): # real signature unknown; restored from __doc__
        """ origin(self) -> QVector3D """
        pass

    def originChanged(self): # real signature unknown; restored from __doc__
        """ originChanged(self) [signal] """
        pass

    def scaleChanged(self): # real signature unknown; restored from __doc__
        """ scaleChanged(self) [signal] """
        pass

    def setOrigin(self, QVector3D): # real signature unknown; restored from __doc__
        """ setOrigin(self, QVector3D) """
        pass

    def setXScale(self, p_float): # real signature unknown; restored from __doc__
        """ setXScale(self, float) """
        pass

    def setYScale(self, p_float): # real signature unknown; restored from __doc__
        """ setYScale(self, float) """
        pass

    def setZScale(self, p_float): # real signature unknown; restored from __doc__
        """ setZScale(self, float) """
        pass

    def xScale(self): # real signature unknown; restored from __doc__
        """ xScale(self) -> float """
        return 0.0

    def xScaleChanged(self): # real signature unknown; restored from __doc__
        """ xScaleChanged(self) [signal] """
        pass

    def yScale(self): # real signature unknown; restored from __doc__
        """ yScale(self) -> float """
        return 0.0

    def yScaleChanged(self): # real signature unknown; restored from __doc__
        """ yScaleChanged(self) [signal] """
        pass

    def zScale(self): # real signature unknown; restored from __doc__
        """ zScale(self) -> float """
        return 0.0

    def zScaleChanged(self): # real signature unknown; restored from __doc__
        """ zScaleChanged(self) [signal] """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


