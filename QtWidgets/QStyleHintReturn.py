# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


class QStyleHintReturn(): # skipped bases: <class 'sip.simplewrapper'>
    """
    QStyleHintReturn(version: int = QStyleOption.Version, type: int = QStyleHintReturn.SH_Default)
    QStyleHintReturn(QStyleHintReturn)
    """
    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    SH_Default = 61440
    SH_Mask = 61441
    SH_Variant = 61442
    Type = 61440
    Version = 1


