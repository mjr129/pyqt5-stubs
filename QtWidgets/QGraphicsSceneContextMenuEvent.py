# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QGraphicsSceneEvent import QGraphicsSceneEvent

class QGraphicsSceneContextMenuEvent(QGraphicsSceneEvent):
    # no doc
    def modifiers(self): # real signature unknown; restored from __doc__
        """ modifiers(self) -> Qt.KeyboardModifiers """
        pass

    def pos(self): # real signature unknown; restored from __doc__
        """ pos(self) -> QPointF """
        pass

    def reason(self): # real signature unknown; restored from __doc__
        """ reason(self) -> QGraphicsSceneContextMenuEvent.Reason """
        pass

    def scenePos(self): # real signature unknown; restored from __doc__
        """ scenePos(self) -> QPointF """
        pass

    def screenPos(self): # real signature unknown; restored from __doc__
        """ screenPos(self) -> QPoint """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    Keyboard = 1
    Mouse = 0
    Other = 2


