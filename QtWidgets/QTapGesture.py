# encoding: utf-8
# module PyQt5.QtWidgets
# from /opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/PyQt5/QtWidgets.so
# by generator 1.145
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui


from .QGesture import QGesture

class QTapGesture(QGesture):
    """ QTapGesture(parent: QObject = None) """
    def position(self): # real signature unknown; restored from __doc__
        """ position(self) -> QPointF """
        pass

    def setPosition(self, Union, QPointF=None, QPoint=None): # real signature unknown; restored from __doc__
        """ setPosition(self, Union[QPointF, QPoint]) """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


