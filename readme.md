PyQt5-Stubs
===========


Stub files for PyQt5.

The are as the base, but contain added information which avoids errors, such as decorating signals with `@property` and methods such as `QMessageBox.getText` with `@static.`

